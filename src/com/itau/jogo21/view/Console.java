package com.itau.jogo21.view;

import java.util.List;
import java.util.Scanner;

import com.itau.jogo21.business.Jogo;
import com.itau.jogo21.dtos.Carta;

public class Console {
  private static Scanner scanner = new Scanner(System.in);
  
  public static boolean querJogar() {
    imprimir("Deseja continuar? (s para sim)");
    
    String opcao = scanner.nextLine();
    
    if(opcao.equals("s")) {
      return true;
    }
    
    return false;
  }
  
  public static void imprimirJogada(Carta carta, Jogo jogo) {
    imprimir("A carta sorteada foi: " + carta);
    imprimir("Sua pontuação é : " + jogo.getPontuacao());
  }
  
  public static void imprimirJogada(List<Carta> cartas, Jogo jogo) {
    imprimir("As cartas sorteadas foram: ");
    
    for (Carta carta : cartas) {
      imprimir(carta);
    }
    
    imprimir("Sua pontuação é : " + jogo.getPontuacao());
  }
  
  public static void imprimirJogoFinalizado(Jogo jogo) {
    if(jogo.eVitoria()) {
      imprimir("Você venceu");
      return;
    }
    
    imprimir("Você perdeu, trouxa");
  }
  
  private static void imprimir(Object texto) {
    System.out.println(texto + "\n");
  }
}
